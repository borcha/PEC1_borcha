var gulp        = require('gulp'),
    del         = require('del'),
    imagemin    = require('gulp-imagemin'),
    sass        = require('gulp-sass'),
    sourcemaps  = require('gulp-sourcemaps'),
    jshint      = require('gulp-jshint'),
    stylish     = require('jshint-stylish'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    browserSync = require('browser-sync');

var srcPaths = {
    images:   'src/img/',
    scripts:  'src/js/',
    styles:   'src/scss/',
    vendor:   'src/vendor/',
    data:     'src/data/',
    files:    'src/'
};

var distPaths = {
    images:   'dist/img/',
    scripts:  'dist/js/',
    styles:   'dist/css/',
    vendor:   'dist/vendor/',
    data:     'dist/data/',
    files:    'dist/'
};

// Limpieza del directorio dist
gulp.task('clean', (cb) =>
    del([ distPaths.files+'**/*'], cb)
);

// Se copian fichero estáticos
gulp.task('copy', (cb) => {
    gulp.src(srcPaths.files+'*.html')
        .pipe(gulp.dest(distPaths.files))
        .pipe(browserSync.stream());
    gulp.src(srcPaths.vendor+'**/**')
        .pipe(gulp.dest(distPaths.vendor))
        .pipe(browserSync.stream());
    gulp.src(srcPaths.data+'**/*')
        .pipe(gulp.dest(distPaths.data))
        .pipe(browserSync.stream());
    cb();
});

// Se comprimen las imágenes
gulp.task('imagemin', () =>
    gulp.src(srcPaths.images+'*')
        .pipe(imagemin())
        .pipe(gulp.dest(distPaths.images))
        .pipe(browserSync.stream())
);

// Se procesan scss
gulp.task('scss', () =>
    gulp.src(srcPaths.styles+'base.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle:'compressed'}))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.styles))
        .pipe(browserSync.stream())
);

// Comprobación de errores js
gulp.task('lint', () =>
    gulp.src(srcPaths.scripts+'**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
);

// Optimización javascript
gulp.task('js', gulp.series('lint', () =>
    gulp.src(srcPaths.scripts+'**/*.js')
        .pipe(sourcemaps.init())
            .pipe(concat('all.min.js'))       
            .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.scripts))
        .pipe(browserSync.stream())
));

// Servir aplicación
gulp.task('serve', gulp.series('copy', 'imagemin', 'scss', 'js', () => {
    browserSync({
        proxy: 'localhost/contacts-agenda/dist'
    });
    gulp.watch([
        srcPaths.files+'*.html',
        srcPaths.vendor+'**/**',
        srcPaths.data+'**/*'
    ], gulp.series('copy'));
    gulp.watch(srcPaths.images+'*', gulp.series('imagemin'));
    gulp.watch(srcPaths.styles+'**/*', gulp.series('scss'));
    gulp.watch(srcPaths.scripts+'**/*.js', gulp.series('js'));
}));

gulp.task('default', gulp.series('clean', 'serve'))
